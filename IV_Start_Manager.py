#!/usr/bin/env python
# coding=utf-8

from tkinter import *
# from tkinter.ttk import *
import platform
import os
import subprocess
from IV_Setting import InfoIni


class MainForm():
    def __init__(self):
        self.master = Tk()
        self.master.title('IV Start Manager 0.5')
        width, height = 640, 480
        screenwidth = self.master.winfo_screenwidth()
        screenheight = self.master.winfo_screenheight()
        size = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.master.geometry(size)


        self.info_ini = InfoIni()
        self.esr_pid = None
        if 'IV_Program_Path' in self.info_ini.info.keys():
            Button(self.master, text="Start_All", font = ('Times',16, 'bold'), command=self.do_start_All_Program,
                   width=12, height=8).grid(row=0, rowspan=3, column=0, pady=10, padx=30)
            Button(self.master, text="Shut_All", font = ('Times',16, 'bold'), command=self.do_shut_All_Program,
                   width=12, height=8).grid(row=3, rowspan=3, column=0, pady=10, padx=30)
            for i, (name, path) in enumerate(self.info_ini.info['IV_Program_Path'].items()):
                Button(self.master, text=str.upper(name), font = ('Arial',11),
                       command=lambda para=path:self.do_start_Program(para),width=12, height=3, anchor='w').\
                       grid(row=i if i < 7 else i-7, column=int(i/7+1), pady=5, padx=10)

    def show(self):
        self.master.mainloop()


    def do_start_Program(self, program_path):
        # terminal = subprocess.Popen('lsmod', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p=subprocess.Popen(eval('program_path'))
        # p = os.popen(eval('program_path'))
        self.esr_pid = p.pid
        print('start IV_Program', program_path)

    def do_start_All_Program(self):
        print("start all")

    def do_shut_All_Program(self):
        if self.esr_pid:
            print('esr_pid', self.esr_pid)
            os.system('kill %s' %self.esr_pid)
        print("shut all")



def main():
    os.popen('echo "000000" | sudo rmmod intel_powerclamp')
    mainform = MainForm()
    mainform.show()


if __name__ == '__main__':
    main()
