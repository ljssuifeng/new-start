# ! /home/lvchi/anaconda3/bin/python
# coding:utf-8

import configparser


class InfoIni(object):
    def __init__(self):
        self.info = None
        self.load_setting()

    def load_setting(self):
        parameters = configparser.ConfigParser()
        try:
            parameters.read("setting.ini")
            self.info = dict(parameters._sections)
            for section in self.info:
                self.info[section] = dict(self.info[section])
        except IOError:
            print(" io read error")


def main():
    info_ini = InfoIni()
    for sec, info in info_ini.info.items():
        if 'Sensor' in sec:
            print(sec)


if __name__ == '__main__':
    main()
